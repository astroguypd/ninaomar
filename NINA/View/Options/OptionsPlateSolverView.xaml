﻿<!--
    Copyright © 2016 - 2019 Stefan Berg <isbeorn86+NINA@googlemail.com>

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    N.I.N.A. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    N.I.N.A. is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with N.I.N.A..  If not, see http://www.gnu.org/licenses/.-->
<UserControl
    x:Class="NINA.View.OptionsPlateSolverView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:enum="clr-namespace:NINA.Utility.Enum"
    xmlns:local="clr-namespace:NINA.View"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:ns="clr-namespace:NINA.Locale"
    xmlns:rules="clr-namespace:NINA.Utility.ValidationRules"
    xmlns:util="clr-namespace:NINA.Utility"
    d:DesignHeight="300"
    d:DesignWidth="300"
    mc:Ignorable="d">
    <Grid Margin="0,5,0,0">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="*" />
        </Grid.RowDefinitions>
        <GroupBox Header="{ns:Loc LblPlateSolving}">
            <Grid>
                <Grid.RowDefinitions>
                    <RowDefinition />
                    <RowDefinition />
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition />
                    <ColumnDefinition />
                </Grid.ColumnDefinitions>
                <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                    <TextBlock
                        Width="140"
                        VerticalAlignment="Center"
                        Text="{ns:Loc LblPlateSolver}" />
                    <ComboBox
                        MinWidth="150"
                        Margin="10,0,0,0"
                        ItemsSource="{Binding Source={util:EnumBindingSource {x:Type enum:PlateSolverEnum}}}"
                        SelectedItem="{Binding PlateSolverType}" />
                </StackPanel>
                <StackPanel
                    Grid.Row="1"
                    Margin="0,5,0,0"
                    Orientation="Horizontal">
                    <TextBlock
                        Width="140"
                        VerticalAlignment="Center"
                        Text="{ns:Loc LblBlindSolver}" />
                    <ComboBox
                        MinWidth="150"
                        Margin="10,0,0,0"
                        ItemsSource="{Binding Source={util:EnumBindingSource {x:Type enum:BlindSolverEnum}}}"
                        SelectedItem="{Binding BlindSolverType}" />
                </StackPanel>

                <StackPanel Grid.RowSpan="2" Grid.Column="1">
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="140"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblExposureTime}" />
                        <TextBox
                            Width="120"
                            Height="25"
                            HorizontalAlignment="Left">
                            <TextBox.Text>
                                <Binding Mode="TwoWay" Path="PlateSolveExposureTime">
                                    <Binding.ValidationRules>
                                        <rules:GreaterZeroRule />
                                    </Binding.ValidationRules>
                                </Binding>
                            </TextBox.Text>
                        </TextBox>
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="140"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblFilter}" />
                        <ComboBox
                            Height="25"
                            DisplayMemberPath="Name"
                            ItemsSource="{Binding FilterWheelFilters}"
                            SelectedItem="{Binding PlateSolveFilter, Mode=TwoWay}"
                            SelectedValuePath="Name" />
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="140"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblErrorLT}" />
                        <TextBox
                            Width="120"
                            Height="25"
                            HorizontalAlignment="Left"
                            VerticalAlignment="Center">
                            <TextBox.Text>
                                <Binding Mode="TwoWay" Path="PlateSolveThreshold">
                                    <Binding.ValidationRules>
                                        <rules:GreaterZeroRule />
                                    </Binding.ValidationRules>
                                </Binding>
                            </TextBox.Text>
                        </TextBox>
                        <TextBlock
                            Margin="5,0,0,0"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblArcmin}" />
                    </StackPanel>
                    <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                        <TextBlock
                            MinWidth="140"
                            MinHeight="20"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblRotationTolerance}" />
                        <TextBox
                            Width="120"
                            Height="25"
                            HorizontalAlignment="Left"
                            VerticalAlignment="Center">
                            <TextBox.Text>
                                <Binding Mode="TwoWay" Path="PlateSolveRotationTolerance">
                                    <Binding.ValidationRules>
                                        <rules:GreaterZeroRule />
                                    </Binding.ValidationRules>
                                </Binding>
                            </TextBox.Text>
                        </TextBox>
                        <TextBlock
                            Margin="5,0,0,0"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblDegree}" />
                    </StackPanel>
                </StackPanel>
            </Grid>
        </GroupBox>

        <GroupBox
            Grid.Row="1"
            Margin="0,10,0,0"
            Header="{ns:Loc LblPlateSolveSettings}">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto" />
                    <ColumnDefinition />
                </Grid.ColumnDefinitions>
                <ListView
                    x:Name="PART_PlateSolverList"
                    MinWidth="180"
                    ItemsSource="{Binding Source={util:EnumBindingSource {x:Type enum:PlateSolverEnum}}}"
                    SelectedIndex="0">
                    <ListView.Resources>
                        <Style x:Key="myHeaderStyle" TargetType="{x:Type GridViewColumnHeader}">
                            <Setter Property="Visibility" Value="Collapsed" />
                        </Style>
                    </ListView.Resources>
                    <ListView.View>
                        <GridView ColumnHeaderContainerStyle="{StaticResource myHeaderStyle}">
                            <GridViewColumn
                                Width="Auto"
                                DisplayMemberBinding="{Binding}"
                                Header="" />
                        </GridView>
                    </ListView.View>
                </ListView>

                <Grid Grid.Column="1" Margin="20,0,0,0">
                    <!--  Astrometry.Net  -->
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition />
                            <ColumnDefinition />
                        </Grid.ColumnDefinitions>
                        <Grid.Style>
                            <Style TargetType="{x:Type Grid}">
                                <Style.Triggers>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="1">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="2">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="3">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                </Style.Triggers>
                            </Style>
                        </Grid.Style>
                        <StackPanel Orientation="Vertical">
                            <StackPanel Margin="0,6,0,6" Orientation="Horizontal">
                                <TextBlock
                                    Width="130"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblAPIKey}" />
                                <TextBox
                                    Grid.Column="1"
                                    MinWidth="130"
                                    HorizontalAlignment="Stretch"
                                    VerticalAlignment="Center"
                                    Text="{Binding AstrometryAPIKey}" />

                                <TextBlock VerticalAlignment="Center" Text="">
                                    <Hyperlink Command="{Binding OpenWebRequestCommand}" CommandParameter="http://nova.astrometry.net/api_help">
                                        <Run Text="?" />
                                    </Hyperlink>
                                </TextBlock>
                            </StackPanel>
                            <StackPanel MaxHeight="150" Margin="0,6,0,6">
                                <TextBlock Text="{ns:Loc LblPlatesolveProvidedBy}" />
                                <TextBlock MaxHeight="73" HorizontalAlignment="Left">
                                    <Hyperlink Command="{Binding OpenWebRequestCommand}" CommandParameter="http://nova.astrometry.net/">
                                        <Image Source="/NINA;component/Resources/astrometry.net.png" Stretch="Uniform" />
                                    </Hyperlink>
                                </TextBlock>
                            </StackPanel>
                        </StackPanel>
                    </Grid>

                    <!--  Local  -->
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition />
                            <ColumnDefinition />
                        </Grid.ColumnDefinitions>
                        <Grid.Style>
                            <Style TargetType="{x:Type Grid}">
                                <Style.Triggers>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="0">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="2">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="3">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                </Style.Triggers>
                            </Style>
                        </Grid.Style>
                        <StackPanel Orientation="Vertical">
                            <Grid Margin="0,6,0,6" VerticalAlignment="Center">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                </Grid.ColumnDefinitions>

                                <TextBlock
                                    Width="130"
                                    HorizontalAlignment="Left"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblCygwinDirectory}" />
                                <TextBox
                                    Grid.Column="1"
                                    Grid.ColumnSpan="2"
                                    MinWidth="130"
                                    HorizontalAlignment="Stretch"
                                    VerticalAlignment="Center">
                                    <TextBox.Text>
                                        <Binding Path="CygwinLocation" UpdateSourceTrigger="LostFocus">
                                            <Binding.ValidationRules>
                                                <rules:DirectoryExistsRule />
                                            </Binding.ValidationRules>
                                        </Binding>
                                    </TextBox.Text>
                                </TextBox>

                                <Button
                                    Grid.Column="3"
                                    Width="30"
                                    Height="30"
                                    Margin="5,0,0,0"
                                    HorizontalAlignment="Left"
                                    VerticalAlignment="Center"
                                    Command="{Binding OpenCygwinFileDiagCommand}">
                                    <Path
                                        Margin="4,17,4,4"
                                        Data="{StaticResource DotsSVG}"
                                        Fill="{StaticResource ButtonForegroundBrush}"
                                        Stretch="Uniform" />
                                </Button>
                            </Grid>
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock
                                        Width="130"
                                        HorizontalAlignment="Left"
                                        VerticalAlignment="Center"
                                        Text="{ns:Loc LblSearchRadius}" />
                                    <TextBox
                                        MinWidth="80"
                                        HorizontalAlignment="Stretch"
                                        VerticalAlignment="Center">
                                        <TextBox.Text>
                                            <Binding Path="AnsvrSearchRadius" UpdateSourceTrigger="LostFocus">
                                                <Binding.ValidationRules>
                                                    <rules:GreaterZeroRule />
                                                </Binding.ValidationRules>
                                            </Binding>
                                        </TextBox.Text>
                                    </TextBox>
                                </UniformGrid>
                            </UniformGrid>

                            <StackPanel MaxHeight="150" Margin="0,5,0,5">
                                <TextBlock Text="{ns:Loc LblPlatesolveProvidedBy}" />
                                <TextBlock MaxHeight="73" HorizontalAlignment="Left">
                                    <Hyperlink Command="{Binding OpenWebRequestCommand}" CommandParameter="http://nova.astrometry.net/">
                                        <Image Source="/NINA;component/Resources/astrometry.net.png" Stretch="Uniform" />
                                    </Hyperlink>
                                </TextBlock>
                            </StackPanel>
                        </StackPanel>
                        <Grid Grid.Column="1">
                            <Grid.RowDefinitions>
                                <RowDefinition />
                                <RowDefinition Height="40" />
                            </Grid.RowDefinitions>
                            <ListView ItemsSource="{Binding IndexFiles}">
                                <ListView.View>
                                    <GridView>
                                        <GridViewColumn
                                            Width="Auto"
                                            DisplayMemberBinding="{Binding}"
                                            Header="{ns:Loc LblIndexFile}" />
                                    </GridView>
                                </ListView.View>
                            </ListView>
                            <Button Grid.Row="1" Command="{Binding DownloadIndexesCommand}">
                                <Button.Content>
                                    <TextBlock Foreground="{StaticResource ButtonForegroundBrush}" Text="{ns:Loc LblDownloadMoreIndexFiles}" />
                                </Button.Content>
                            </Button>
                        </Grid>
                    </Grid>

                    <!--  Platesolve2  -->
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition />
                            <ColumnDefinition />
                        </Grid.ColumnDefinitions>
                        <Grid.Style>
                            <Style TargetType="{x:Type Grid}">
                                <Style.Triggers>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="0">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="1">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="3">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                </Style.Triggers>
                            </Style>
                        </Grid.Style>
                        <StackPanel Orientation="Vertical">
                            <Grid Margin="0,6,0,6" VerticalAlignment="Center">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                </Grid.ColumnDefinitions>

                                <TextBlock
                                    Width="130"
                                    HorizontalAlignment="Left"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblPlatesolve2Directory}" />
                                <TextBox
                                    Grid.Column="1"
                                    Grid.ColumnSpan="2"
                                    MinWidth="130"
                                    HorizontalAlignment="Stretch"
                                    VerticalAlignment="Center">
                                    <TextBox.Text>
                                        <Binding Path="PS2Location" UpdateSourceTrigger="LostFocus">
                                            <Binding.ValidationRules>
                                                <rules:FileExistsRule />
                                            </Binding.ValidationRules>
                                        </Binding>
                                    </TextBox.Text>
                                </TextBox>

                                <Button
                                    Grid.Column="3"
                                    Width="30"
                                    Height="30"
                                    Margin="5,0,0,0"
                                    HorizontalAlignment="Left"
                                    VerticalAlignment="Center"
                                    Command="{Binding OpenPS2FileDiagCommand}">
                                    <Path
                                        Margin="4,17,4,4"
                                        Data="{StaticResource DotsSVG}"
                                        Fill="{StaticResource ButtonForegroundBrush}"
                                        Stretch="Uniform" />
                                </Button>
                            </Grid>
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock
                                        Width="130"
                                        HorizontalAlignment="Left"
                                        VerticalAlignment="Center"
                                        Text="{ns:Loc LblRegions}" />
                                    <TextBox
                                        MinWidth="80"
                                        HorizontalAlignment="Stretch"
                                        VerticalAlignment="Center">
                                        <TextBox.Text>
                                            <Binding Path="PS2Regions" UpdateSourceTrigger="LostFocus">
                                                <Binding.ValidationRules>
                                                    <rules:GreaterZeroRule />
                                                </Binding.ValidationRules>
                                            </Binding>
                                        </TextBox.Text>
                                    </TextBox>
                                </UniformGrid>
                            </UniformGrid>
                        </StackPanel>
                    </Grid>

                    <!--  AllSkyPlateSolver  -->
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition />
                            <ColumnDefinition />
                        </Grid.ColumnDefinitions>
                        <Grid.Style>
                            <Style TargetType="{x:Type Grid}">
                                <Style.Triggers>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="0">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="1">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                    <DataTrigger Binding="{Binding ElementName=PART_PlateSolverList, Path=SelectedIndex}" Value="2">
                                        <Setter Property="Visibility" Value="Collapsed" />
                                    </DataTrigger>
                                </Style.Triggers>
                            </Style>
                        </Grid.Style>
                        <StackPanel Orientation="Vertical">
                            <Grid Margin="0,6,0,6" VerticalAlignment="Center">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                </Grid.ColumnDefinitions>

                                <TextBlock
                                    Width="130"
                                    HorizontalAlignment="Left"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblASPSDirectory}" />
                                <TextBox
                                    Grid.Column="1"
                                    Grid.ColumnSpan="2"
                                    MinWidth="130"
                                    HorizontalAlignment="Stretch"
                                    VerticalAlignment="Center">
                                    <TextBox.Text>
                                        <Binding Path="AspsLocation" UpdateSourceTrigger="LostFocus">
                                            <Binding.ValidationRules>
                                                <rules:DirectoryExistsRule />
                                            </Binding.ValidationRules>
                                        </Binding>
                                    </TextBox.Text>
                                </TextBox>

                                <Button
                                    Grid.Column="3"
                                    Width="30"
                                    Height="30"
                                    Margin="5,0,0,0"
                                    HorizontalAlignment="Left"
                                    VerticalAlignment="Center"
                                    Command="{Binding OpenASPSFileDiagCommand}">
                                    <Path
                                        Margin="4,17,4,4"
                                        Data="{StaticResource DotsSVG}"
                                        Fill="{StaticResource ButtonForegroundBrush}"
                                        Stretch="Uniform" />
                                </Button>
                            </Grid>
                            <UniformGrid Columns="2">
                                <UniformGrid
                                    Margin="0,6,0,6"
                                    VerticalAlignment="Center"
                                    Columns="2">
                                    <TextBlock
                                        Width="130"
                                        HorizontalAlignment="Left"
                                        VerticalAlignment="Center"
                                        Text="{ns:Loc LblSearchRadius}" />
                                    <TextBox
                                        MinWidth="80"
                                        HorizontalAlignment="Stretch"
                                        VerticalAlignment="Center">
                                        <TextBox.Text>
                                            <Binding Path="AnsvrSearchRadius" UpdateSourceTrigger="LostFocus">
                                                <Binding.ValidationRules>
                                                    <rules:GreaterZeroRule />
                                                </Binding.ValidationRules>
                                            </Binding>
                                        </TextBox.Text>
                                    </TextBox>
                                </UniformGrid>
                            </UniformGrid>
                        </StackPanel>
                    </Grid>
                </Grid>
            </Grid>
        </GroupBox>
    </Grid>
</UserControl>